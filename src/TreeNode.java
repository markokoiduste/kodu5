import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode() {
   }

   TreeNode (String n, TreeNode d, TreeNode r) {
      setName(n);
      setNextSibling(d);
      setFirstChild(r);
   }

   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setNextSibling(TreeNode t) {
      nextSibling = t;
   }

   public TreeNode getNextSibling() {
      return nextSibling;
   }

   public void setFirstChild(TreeNode t) {
      firstChild = t;
   }

   public TreeNode getFirstChild() {
      return firstChild;
   }

   public static TreeNode parsePrefix (String s) {
      checkPrefix(s);
      return buildTree(s);
   }

   private static TreeNode buildTree(String s) {
      if (s.isEmpty()) return null;
      subCheck(s);
      TreeNode output = new TreeNode();
      String childBuffer = "";
      String siblingBuffer = "";
      String delimBuffer = "";
      StringTokenizer tokens = new StringTokenizer(s,"(),",true);

      output.setName(tokens.nextToken());
      int n = 0;
      while(tokens.hasMoreTokens()) {
         delimBuffer = tokens.nextToken();
         if (delimBuffer.equals("(")) {
            if (n==0) childBuffer=tokens.nextToken();
            else childBuffer+=delimBuffer+tokens.nextToken();
            n++;
         }
         else if (delimBuffer.equals(",")) {
            if (n!=0) childBuffer+=delimBuffer+tokens.nextToken();
            else while (tokens.hasMoreTokens()) {
               siblingBuffer+=tokens.nextToken();
            }
         }
         else if (delimBuffer.equals(")")) {
            n--;
            if (n>0) childBuffer+=delimBuffer;
         }
      }
      output.setFirstChild(TreeNode.buildTree(childBuffer));
      output.setNextSibling(TreeNode.buildTree(siblingBuffer));
      return output;
   }


   private static boolean checkPrefix(String s) {
      if (s.isEmpty()) throw new RuntimeException("Empty input: '"+s+"'");
      subCheck(s);
      int opening = s.length()-s.replace("(","").length();
      int closing = s.length()-s.replace(")","").length();
      int commas = s.length()-s.replace(",","").length();
      if (opening != closing) throw new RuntimeException("Brackets not even: '"+s+"'");
      if ((opening * closing)==0 && commas!=0) throw new RuntimeException("Input without brackets: '"+s+"'");
      if (s.length()-s.replace(" ","").length()>0) throw new RuntimeException("Spaces in input: "+s+"'");
      return true;
   }

   private static boolean subCheck(String s) {
      if (s==null) throw new RuntimeException("Null string");
      if (s.charAt(s.indexOf(",")+1)==',') throw new RuntimeException("Double commas: '"+s+"'");
      if (s.charAt(s.indexOf("(")+1)=='(') throw new RuntimeException("Double brackets: '"+s+"'");
      if (s.charAt(s.indexOf("(")+1)==')') throw new RuntimeException("Empty subtree: '"+s+"'");
      return true;
   }

   public String rightParentheticRepresentation() {
      StringBuffer buffer = new StringBuffer();
      if (getFirstChild()!=null){
         buffer.append("(");
         buffer.append(getFirstChild().rightParentheticRepresentation());
         buffer.append(")");
      }
      buffer.append(getName());
      if (getNextSibling()!=null) {
         buffer.append(",");
         buffer.append(getNextSibling().rightParentheticRepresentation());
      }
      return buffer.toString();
   }

   public static void main (String[] param) {
      String s = "A(B1,C)";
      TreeNode t = TreeNode.parsePrefix (s);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C) ==> (B1,C)A
   }
}